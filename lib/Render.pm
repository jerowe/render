package Render;

use 5.010000;
use Carp;
use Class::MOP;
use Data::Dumper;
use Moose;

require Exporter;

our $VERSION = '0.01';

sub BUILDARGS {
    my $class = shift;

    if ( scalar @_ == 1 && ref( $_[0]) ne 'HASH' ) {
        my $arg = $_[0];
        return blessed($arg) ? { infile => $arg } : { infile => $arg };
    }
    return $class->SUPER::BUILDARGS(@_);
}

sub BUILD {
    my $self = shift;

    return;
}

sub before_build {}
sub after_build {}


__PACKAGE__->meta->make_immutable;
1;
__END__

=head1 NAME

Render - empty base class

=head1 SYNOPSIS

  use Moose;
  extends 'Render';
  with 'otherMooseRole';

=head1 DESCRIPTION

Empty base class

=head2 EXPORT

None by default.

=head1 SEE ALSO

Render::TextCSV_XS
Render::TextCSV_XS::MongoDB

=head1 AUTHOR

Jillian Rowe, E<lt>jillian.e.rowe@gmail.com<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Jillian Rowe

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
